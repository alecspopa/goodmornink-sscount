<?php
/**
 * @package Social Share Count
 * @version 1.0
 * 
 * 
 * Plugin Name: Social Share Count
 * Plugin URI: https://bitbucket.org/alecspopa/goodmornink-sscount/overview
 * Description: Count the number of shares on varius social networks
 * Version: 1.0
 * Author: alecs popa
 * Author URI: http://alecspopa.com/
 * License: GPL2
 * 
 * 
 * Copyright 2012 alecspopa (email: contact@alecspopa.com)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as 
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

define('SSC_UPDATE', 3600); // set the update time to 1 hour

global $ssc_db_version;
$ssc_db_version = '1.0';

// Create required table at activation
function ssc_install() {
	global $wpdb;
	$table_name = $wpdb->prefix . 'sscount';
	
	$sql = "CREATE TABLE sscount (
			  network varchar(50) NOT NULL,
			  url varchar(255) NOT NULL,
			  count int(11) NOT NULL,
			  timestamp int(11) NOT NULL,
			  PRIMARY KEY  (network, url),
			  KEY url (url)
			);";
			
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);
	
	add_option('ssc_db_version', $ssc_db_version);
	
	// add rewrite rules
	global $wp_rewrite;
	sscount_rewrite();
	$wp_rewrite->flush_rules(true);
}

// Remove the required table at deactivation
// NOTE: the table is removed at deactivation and not at 'delete' because many people just delete the plugin from FTP 
function ssc_uninstall() {
	global $wpdb;
	$table_name = $wpdb->prefix . 'sscount';
	
	$wpdb->query("DROP TABLE {$table_name}");
	
	delete_option('ssc_db_version');
}

// Register the 2 function above in WP
register_activation_hook(__FILE__, 'ssc_install');
register_deactivation_hook(__FILE__, 'ssc_uninstall');

// Register an url for the count callback
add_action('init', 'sscount_rewrite' );
function sscount_rewrite() {
	//$_POST = $_GET; // DEBUG
	
	if (isset($_POST['permaLink'])) {
		$network = '';
		$res = 0;
		
		if (preg_match('/sscount-stumbleupon/', $_SERVER['REQUEST_URI'])) {
			$res = SSCount::get('stumbleupon', urldecode($_POST['permaLink']));
		}
		else if (preg_match('/sscount-facebook/', $_SERVER['REQUEST_URI'])) {
			$res = SSCount::get('facebook', urldecode($_POST['permaLink']));
		}
		else if (preg_match('/sscount-googleplus/', $_SERVER['REQUEST_URI'])) {
			$res = SSCount::get('googleplus', urldecode($_POST['permaLink']));
		}
		else if (preg_match('/sscount-pinterest/', $_SERVER['REQUEST_URI'])) {
			$res = SSCount::get('pinterest', urldecode($_POST['permaLink']));
		}
		
		die(json_encode($res));
	}
}

// The plugin class
class SSCount {
	protected static $counts = array();
	
	/**
	 * Get count function
	 * Will return the count from db and update it if it is older that SSC_UPDATE
	 * 
	 * @author alecs popa
	 * @version 1
	 * @since 2012.06.23
	 * 
	 * @param string - social network from where to get the count
	 * @param string - the url that was shared
	 * 
	 * @return int - the count
	 */
	public static function get($network, $url) {
		if (WP_DEV) { $url = str_replace('dev.', '', $url);	} // DEBUG
		
		// new counts? ohh goodie
		if (!isset(SSCount::$counts[$network][$url]['count'])) {
			// get the count from db		
			global $wpdb;
			$table_name = $wpdb->prefix . 'sscount';
			
			$sql = $wpdb->prepare("SELECT network, count, timestamp FROM $table_name WHERE url = %s", $url);
			$res = $wpdb->get_results($sql, ARRAY_A);

			if (is_array($res)) {
				foreach ($res as $c) {
					SSCount::$counts[ $c['network'] ][$url] = array('count' => $c['count'], 'timestamp' => $c['timestamp']);
				}
			}
		}
		
		// update the count if no counts were found in db or if they are old
		$have_network_count = isset(SSCount::$counts[$network][$url]['timestamp']);
		if ( !$have_network_count || ($have_network_count && SSCount::$counts[$network][$url]['timestamp'] <= (time() - SSC_UPDATE)) ) {
			switch($network) {
				case 'twitter':
					$data = SSCount::_curl_request('http://urls.api.twitter.com/1/urls/count.json?url=' . $url);
					$djson = json_decode($data);

					if ($djson !== false && isset($djson->count)) {
						SSCount::$counts[$network][$url]['count'] = intval($djson->count);

						// insert new data or update old one
						SSCount::_update_count($network, $url, $djson->count);
					}
				break;
				
				case 'facebook':
					$data = SSCount::_curl_request('http://graph.facebook.com/?id=' . $url);
					$djson = json_decode($data);

					if ($djson !== false && isset($djson->shares)) {
						SSCount::$counts[$network][$url]['count'] = intval($djson->shares);

						// insert new data or update old one
						SSCount::_update_count($network, $url, SSCount::$counts[$network][$url]['count']);
					}
				break;
				
				case 'stumbleupon':
					$data = SSCount::_curl_request('http://www.stumbleupon.com/services/1.01/badge.getinfo?url=' . $url);
					$djson = json_decode($data);
					
					if ($djson !== false && isset($djson->result->views)) {
						SSCount::$counts[$network][$url]['count'] = intval($djson->result->views);

						// insert new data or update old one
						SSCount::_update_count($network, $url, SSCount::$counts[$network][$url]['count']);
					}
				break;
				
				case 'googleplus':
					$opt = array(
						'use_post' => true,
						'post_fields' => '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]'
					);
					$data = SSCount::_curl_request('https://clients6.google.com/rpc', $opt);
					$djson = json_decode($data);
					$count = isset($djson[0]->result->metadata->globalCounts->count) ? intval($djson[0]->result->metadata->globalCounts->count) : null;					
					
					if ($djson !== false && isset($count)) {
						SSCount::$counts[$network][$url]['count'] = intval($count);

						// insert new data or update old one
						SSCount::_update_count($network, $url, $djson->count);
					}
				break;
				
				case 'pinterest':
					$data = SSCount::_curl_request('http://api.pinterest.com/v1/urls/count.json?url=' . $url);
					$data = str_replace(array('receiveCount({', '})'), array('{', '}'), $data); // the response is a json wrapped in some string
					$djson = json_decode($data);
					
					if ($djson !== false && isset($djson->count)) {
						SSCount::$counts[$network][$url]['count'] = intval($djson->count);

						// insert new data or update old one
						SSCount::_update_count($network, $url, $djson->count);
					}
				break;
			}
		}

		return isset(SSCount::$counts[$network][$url]['count']) ? SSCount::_format( SSCount::$counts[$network][$url]['count'] ) : 0;
	}
	
	/**
	 * Utility function for get_count used to made the cURL request form counts
	 * 
	 * @author alecs popa
	 * @version 1
	 * @since 2012.06.23
	 * 
	 * @param string - the url for cURL request
	 * @param array - options used for request
	 * @return mixed - the request result
	 */
	protected static function _curl_request($social_url, array $opt = array()) {
		$cURL = curl_init();
		curl_setopt($cURL, CURLOPT_URL, $social_url);
	    curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($cURL, CURLOPT_CONNECTTIMEOUT ,0); 
		curl_setopt($cURL, CURLOPT_TIMEOUT, 60);
		
		if ( !empty($opt['use_post']) && !empty($opt['post_fields']) ) {
			curl_setopt($cURL, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
			curl_setopt($cURL, CURLOPT_POST, 1);
			curl_setopt($cURL, CURLOPT_POSTFIELDS, $opt['post_fields']);
		}
		else {
			curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, false);
			curl_setopt($cURL, CURLOPT_USERAGENT,"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1" );
			curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
		}

	    $data = curl_exec($cURL);
		curl_close($cURL);
		
		return $data;
	}
	
	/**
	 * Utility function to update the count in the db
	 * 
	 * @author alecs popa
	 * @version 1
	 * @since 2012.06.23
	 * 
	 * @param string - social network from where to get the count
	 * @param string - the url that was shared
	 * @param int - the count for the url
	 * 
	 * @return mixed - the number of affected rows or FALSE on error
	 */
	protected static function _update_count($network, $url, $count) {
		global $wpdb;
		$table_name = $wpdb->prefix . 'sscount';

		$sql = $wpdb->prepare(
			"INSERT INTO $table_name (network, url, count, timestamp) VALUES (%s, %s, %d, UNIX_TIMESTAMP())
				ON DUPLICATE KEY UPDATE count = %d, timestamp = UNIX_TIMESTAMP()", 
				$network, $url, $count, $count
			);
		$res = $wpdb->query($sql);
		
		return $res;
	}
	
	/**
	 * Utility function to format the count with K and M
	 * 
	 * @author alecs popa
	 * @version 1
	 * @since 2012.06.23
	 * 
	 * @param int - the count
	 * @return string - the formated number
	 */
	protected static function _format($count) {
		$formated_count = '';
		
		if ($count > 1000000) {
			$formated_count = number_format($count / 1000000, 0, ',', '.') . 'M';
		}
		else if ($count > 1000) {
			$formated_count = number_format($count / 1000, 0, ',', '.') . 'K';
		}
		else {
			$formated_count = number_format($count, 0, ',', '.');
		}
		
		return $formated_count;
	}
}


